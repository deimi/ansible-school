Syntax check
```bash
ansible-playbook user.yml --syntax-check
```

Dry run without changing anything on the nodes
Attention: not all modules support the check mode. During a dry run this modules get skipped. Following tasks which depend on the output of those skipped task, may then fail.
```bash
ansible-playbook user.yml --check
```