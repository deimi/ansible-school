Check connection with ping module
```bash
ansible all -m ping

ansible webservers -m ping

ansible db02 -m ping
```

Specifiy inventory file, instead of using the one configured in ansible.cfg
```bash
ansible all -i inventory.ini -m ping
```

Ad hoc command with module arguments
```bash
ansible db02 -m user -a 'name=ubuntu state=absent'
```

List all available modules and get more information about it
```bash
ansible-doc -l
ansible-doc ping
```

Limit execution to a specific host
```bash
ansible all --limit web02 -m ping
```

Calling a custom command on the managed node. This is not idempotent, so you have to take car that you can safely call this twice or more without sideeffects.
```bash
ansible all -m command -a "/bin/hostname"
ansible all -m shell -a "/bin/hostname"
```

Calling raw custom commands on managed nodes. This module run directly on the remote shell without the need of Python installed on the managed node. Useful for nodes which don't support python or a way to install python on a node.
```bash
ansible all -m raw -a "/bin/hostname"
```
