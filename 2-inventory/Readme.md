Print a inventory content
```bash
ansible-inventory -i inventory/inventory.ini --list     # plain list in json format
ansible-inventory -i inventory/inventory.ini --graph    # better graphical representation of the inventory

ansible-inventory -i inventory/inventory_ranges.ini --list
ansible-inventory -i inventory/inventory_ranges.ini --graph

ansible-inventory -i inventory/inventory.yml --list
ansible-inventory -i inventory/inventory.yml --graph

ansible-inventory -i inventory/inventory_ranges.yml --list
ansible-inventory -i inventory/inventory_ranges.yml --graph

ansible-inventory -i inventory/ --list     # uses all files in the directory and merges it to one inventory configuration
```

Output in yaml format. Can be used to convert ini to yaml
```bash
ansible-inventory -i inventory/inventory.ini -y --list
```

Use the default inventory setting which is defined in ansible.cfg
``` bash
ansible-inventory --graph
```

Verify if a specific host/group is present in an inventory
```bash
ansible -i inventory.ini webserver1 --list-hosts

ansible -i inventory.ini 172.18.0.* --list-hosts
```