# specific connection variables for this host only

ansible_host: 192.0.2.1     # overwrite the ip address which was defined in the inventory file
ansible_port: 1234          # ssh ansible_port
ansible_user: ubuntu        # user on the managed host
ansible_become: true        # use privileged escalation
ansible_become_user: root   # privileged escalation user
ansible_become_method: su   # method for getting root rights          