Execute a playbook, for all hosts defined in the playbook itself
```bash
ansible-playbook ping.yml
```

Limit execution of playbook to a subset of the hosts which are defined in the playbook
```bash
ansible-playbook ping.yml --limit webservers
```

Syntax check of yaml file
```bash
ansible-playbook --syntax-check ping.yml
```

Dry run, which reports what would occur if executed, but without really executing
```bash
ansible-playbook -C ping.yml
ansible-playbook --check ping.yml
```

Set variable during command line execution
```bash
ansible-playbook -e "username=student" example2.yml
```

ansible-vault for encrypted variables
```bash
ansible-vault create filename       # create a new encrypted file
ansible-vault edit filename         # edit an encrypted file
ansible-vault view filename
ansible-vault encrypt filename      # encrypt an existing file
ansible-vault decrypt filename      # decrypt an existing file
ansible-vault rekey filename        # change password of an encrypted file
```

Calling a playbook with "vaulted" files.  
@prompt tells ansible that it will prompt for the default decryption password  
foo@prompt will show an password prompt for the "foo" vault. Useful when supplying multiple vaults at once.
```bash
ansible-playbook --ask-vault-pass playbook.yml
ansible-playbook --vault-id @prompt playbook.yml
ansible-playbook --vault-id foo@prompt --vault-id bar@prompt playbook.yml
```