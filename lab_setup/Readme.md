```
NAME=webserver11
IP=11
```

```
NAME=webserver12
IP=12
```

```
NAME=database21
IP=21
```

```
NAME=database22
IP=22
```

Terminator broadcast to all
```
docker run -it --rm -h $NAME --name $NAME --net mynetwork --ip 172.18.0.$IP python:latest bash
```